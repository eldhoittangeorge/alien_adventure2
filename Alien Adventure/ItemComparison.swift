//
//  ItemComparison.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

import Foundation

func <(lhs: UDItem, rhs: UDItem) -> Bool {
    var Bool_Value:Bool = true
    if lhs.rarity.rawValue < rhs.rarity.rawValue{
        Bool_Value = true
    }
    else if lhs.rarity.rawValue == rhs.rarity.rawValue{
        if lhs.baseValue < rhs.baseValue{
            Bool_Value = true
        }
        else{
            Bool_Value = false
        }
    }
    else{
        Bool_Value = false
    }
    return Bool_Value
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 5"